package practica2;

//package copiararchivo;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopiarArchivo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        File miDir = new File (".");
        try {
            System.out.println ("Directorio actual: " + miDir.getCanonicalPath());
        }catch(Exception e) {
            e.printStackTrace();
        }


        String rutaFichOrigen="./practica1.bmp";//origen
        String rutaFichDestino="./copia1.bmp"; //destino
        String rutaFichOrigen2="./lachute_dep2.txt";
        File ficheroOrigen=new File(rutaFichOrigen);//objeto con la ruta origen.
        File ficheroDestino=new File(rutaFichDestino);//objeto con la ruta destino.
        File ficheroOrigen2=new File(rutaFichOrigen2);
        BufferedInputStream lectorFichero;//flujo buffer de lectura .
        BufferedOutputStream escritorFichero;//buffer de escritura.
        BufferedInputStream lectorFichero2;
        try{
            lectorFichero=new BufferedInputStream(new FileInputStream(ficheroOrigen));//Inicializa el buffer de lectura con un FileInputStream
            escritorFichero=new BufferedOutputStream(new FileOutputStream(ficheroDestino));//Inicializa el buffer de escritura con un FileOutputStream
            lectorFichero2=new BufferedInputStream(new FileInputStream(ficheroOrigen2));
            int bytes;

            String read, inv;

            int[] intRead, invInt;

            long conv;
            
            //Bytes del tipo del archivo
            intRead = new int[2];
            intRead=recoverNext(2,lectorFichero, escritorFichero);

            System.out.println("Tipo de archivo: ");
            printArrayIntInHex(intRead);
            System.out.println("");

            
            //Tamaño del archivo
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Tamaño del archivo: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");

            
            //Reservados
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Reservados: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Inicio de datos
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Inicio de la imagen: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Tamaño de la cabecera
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Tamaño de la cabecera: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Anchura (px) 
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Anchura (px): ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Altura (px) 
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Alatura (px): ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Num planos (px) 
            intRead = new int[2];
            intRead=recoverNext(2,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("No. planos (px): ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Resolución de cada pixel 
            intRead = new int[2];
            intRead=recoverNext(2,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Resolución de cada pixel: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Tipo de compresion 
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Tipo de compresión: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Tamaño de la imgen 
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Tamaño de la imagen: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Res horizontal 
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Res horizontal: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Res vertical
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Res vertical: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            
            //Tam. paleta de colores
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Tam. paleta de colores: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //No. colores importantes
            intRead = new int[4];
            intRead=recoverNext(4,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("No. colores importantes: ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            //Imagen
            intRead = new int[768];

            intRead=recoverNext(768,lectorFichero2, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Imagen ");
            
            printArrayIntInHex(intRead);
            System.out.println("");
            
            

            //read=recoverNext(2,lectorFichero, escritorFichero);

            //read=recoverNext(2,lectorFichero, escritorFichero);


            lectorFichero.close();
            escritorFichero.close();

        }
        catch(FileNotFoundException e){
            e.printStackTrace();

        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    static int[] recoverNext(int nextBytes,BufferedInputStream lectorFichero, BufferedOutputStream escritorFichero){
        int[] bytesRecovered= new int[nextBytes];
        String byteRead="";
        int count=0;
        int byteRecovered;
        try{
            if(nextBytes>0){
                while(count<nextBytes){
                    if((byteRecovered=lectorFichero.read())!=-1){
                       //System.out.println("Leido: "+byteRecovered);

                       bytesRecovered[count]=byteRecovered;
                       writeToFile(escritorFichero, byteRecovered);
                    }
                    count++;
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return(bytesRecovered);
    }

    static int[] reverseInt(int[] bytes){
        int[] revString= new int[bytes.length];
        int index=0;
        for (int x=bytes.length-1;x>=0;x--){
            revString[index++] = bytes[x];
        }
        return(revString);
    }

    static void printArrayInt(int[] intArray){
        for (int x=0; x<intArray.length;x++){
            System.out.print(intArray[x]+" ");
        }
    }

    static void printArrayIntInHex(int[] intArray){
        for (int x=0; x<intArray.length;x++){
            System.out.print(Integer.toHexString(intArray[x])+" ");
        }
    }

    static long StringToInt(String bytes){
        long value=0;

        value=Long.parseLong(bytes, 16);

        return(value);
    }

    static void writeToFile(BufferedOutputStream escritorFichero, int bytes){
        try{
            escritorFichero.write(bytes);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
