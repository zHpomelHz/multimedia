/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica_mp3;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.naming.Context;
import javax.swing.ImageIcon;
import javax.swing.text.html.HTML;
import org.jaudiotagger.audio.AudioFile;
//import org.farng.mp3.MP3File;
//import org.farng.mp3.TagException;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.TagOptionSingleton;
import org.jaudiotagger.tag.datatype.*;
/**--
 *
 * @author zHalexHz
 */
public class Practica_MP3 {
 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, TagException, CannotReadException, ReadOnlyFileException, InvalidAudioFrameException {
       String Darchivo ="C:\\Users\\zHalexHz\\Documents\\NetBeansProjects\\Practica_MP3\\src\\practica_mp3\\Highway To Hell.mp3";
            File archivo = new File (Darchivo);
        
    
        final AudioFile audioFile = AudioFileIO.read(archivo);
        final Tag tag = audioFile.getTag();
        final AudioHeader header = audioFile.getAudioHeader();

        final byte[] thumbnail;
        final List<Artwork> artwork = tag.getArtworkList();
        if (artwork.isEmpty()) {
            thumbnail = new byte[0];
        } else {
            thumbnail = artwork.get(0).getBinaryData();
        }
        
        NewJFrame o = new NewJFrame();
        o.setVisible(true);
        
       
        o.setEnabled(true);
        o.setLayout(null);
        
        ImageIcon xx = new ImageIcon(thumbnail);
        xx.getImage();
        o.jLabel1.setVisible(true);
        o.jLabel1.setIcon(xx);
    }
 

}



