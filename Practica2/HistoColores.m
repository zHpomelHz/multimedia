close all;
clear all;
clc;
vec_col =["ffffff","ffffff","ffffff","ffffff","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced""241ced","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","ffffff","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","ffffff","ffffff","ffffff","ffffff","4cb122","4cb122","4cb122","4cb122","e8a20","e8a20","e8a20","e8a20","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","4cb122","4cb122","4cb122","4cb122","e8a20","e8a20","e8a20","e8a20","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","4cb122","4cb122","e8a20","e8a20","e8a20","e8a20","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","4cb122","4cb122","4cb122","4cb122","e8a20","e8a20","e8a20","e8a20","4cb122","4cb122","4cb122","4cb122","241ced","241ced","241ced","241ced","241ced","241ced","241ced","241ced","000000","000000","ffffff","ffffff","ffffff","ffffff","e8a20","e8a20","e8a20","e8a20","241ced","241ced","241ced","241ced","000000","000000","ffffff","ffffff","ffffff","ffffff","e8a20","e8a20","e8a20","e8a20","241ced","241ced","241ced","241ced","000000","000000","ffffff","ffffff","ffffff","ffffff","e8a20","e8a20","e8a20","e8a20","241ced","241ced","241ced","241ced","000000","000000","ffffff","ffffff","ffffff","ffffff","e8a20","e8a20","e8a20","e8a20","e8a20","e8a20","e8a20","e8a20","241ced","241ced","241ced","241ced","4cb122","4cb122","4cb122","4cb122","000000","000000","e8a20","e8a20","e8a20","e8a20","241ced","241ced","241ced","241ced","4cb122","4cb122","4cb122","4cb122","000000","000000","e8a20","e8a20","e8a20""e8a20","241ced","241ced","241ced","241ced","4cb122","4cb122","4cb122","4cb122","000000","000000","e8a20","e8a20","e8a20","e8a20","241ced","241ced","241ced","241ced","4cb122","4cb122","4cb122","4cb122","000000","000000" ];
cat_vec_col = categorical({'ffffff','4cb122','241ced','e8a20','000000'});
valores = zeros(1,numel(cat_vec_col));
c = 0;
for n = 1:numel(valores)
    c = 0;
    for n1 = 1:numel(vec_col)
        if cat_vec_col(n) == vec_col(n1)
            c = c + 1
        end
    end
    valores(n) = c;
end
bar(cat_vec_col,valores)
title('Colores');